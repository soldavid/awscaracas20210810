# [Boto3 en AWS, Amazon Quantum Ledger Database - QLDB](https://www.meetup.com/AWS-User-Group-Caracas/events/279547254/)

[![Meetup Invitation](meetup.jpeg)](https://www.meetup.com/AWS-User-Group-Caracas/events/279547254/)

## David Sol

### T-Systems Cloud Architect

### Twitter: [@soldavidcloud](https://twitter.com/soldavidcloud)

### Repositorio: <https://gitlab.com/soldavid/awscaracas20210810>
